import React, { Component } from "react";
import { View, Text } from "react-native";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
        <Text
          onPress={() =>
            this.props.navigation.navigate("TodoList", { method: "BasicRedux" })
          }
        >
          Traditional Redux
        </Text>
        <Text
          onPress={() =>
            this.props.navigation.navigate("TodoList", { method: "ReduxSauce" })
          }
        >
          Redux Sauce
        </Text>
      </View>
    );
  }
}
