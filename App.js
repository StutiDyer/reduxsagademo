/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import { SafeAreaView, StyleSheet, StatusBar } from "react-native";
import AppNavigator from "./app/Navigation/Navigation";
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducer from "./app/Store/index";

const store = createStore(reducer);
const App: () => React$Node = () => {
  return (
    <>
      <Provider store={store}>
        <SafeAreaView style={{ flex: 1 }}>
          <AppNavigator />
        </SafeAreaView>
      </Provider>
    </>
  );
};

export default App;
