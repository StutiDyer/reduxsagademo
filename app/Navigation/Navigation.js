import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import TodoList from '../Containers/TodoListScreen';
import Home from '../Containers/HomeScreen';

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    TodoList: TodoList,
  },
  {
    initialRouteName: 'Home'
  },
);

export default createAppContainer(AppNavigator);
