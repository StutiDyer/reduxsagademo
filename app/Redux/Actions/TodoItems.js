import {ADD_TODO, UPDATE_TODO, DELETE_TODO} from '../Actions/Type'
 

export const addTodo = payload => ({
  type: ADD_TODO,
  payload
});

export const updateTodo = payload => ({
  type: UPDATE_TODO,
  payload
});

export const deleteTodo = payload => ({
  type: DELETE_TODO,
  payload
});
