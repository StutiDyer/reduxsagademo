import { createReducer, createActions } from "reduxsauce";

export const { Types,Creators } = createActions({
    addTodo: ["payload"],
    updateTodo:["payload"],
    deleteTodo:["payload"]
})

const INITIAL_STATE = {
    todoItems:[]
}

const addTodoData = (state = INITIAL_STATE,action) => {
    return {todoItems : [...state.todoItems, action.payload]}
}

const updateTodoData = (state = INITIAL_STATE,action) => {
    const update_todoItem = [...state.todoItems];
    let id = update_todoItem.findIndex(data => data.id === action.payload.id);
    if( id > -1){
        update_todoItem[id] = {...action.payload};
    }
    return {...state,todoItems:update_todoItem}
}

const deleteTodoData = (state = INITIAL_STATE,action) => {
    const item = [...state.todoItems];
    return {todoItems: item.filter(data => data.id != action.payload.id)};
}


const HANDLERS = {
    [Types.ADD_TODO]: addTodoData,
    [Types.UPDATE_TODO]: updateTodoData,
    [Types.DELETE_TODO]: deleteTodoData
};

const reduxSauceReducer = createReducer(INITIAL_STATE,HANDLERS)

export default reduxSauceReducer;