import React, { Component } from "react";
import { View, Text,FlatList } from "react-native";
import {connect} from "react-redux";
import { Creators } from "../ReduxSauce/index";
import {addTodo,updateTodo,deleteTodo} from "../Redux/Actions/TodoItems"
import DialogInput from 'react-native-dialog-input';
import {ListItem,Body,Right} from 'native-base';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDialogVisible:false,
      selectedItem:null
    };
  }
  showDialog = () => {
    this.setState({ isDialogVisible: true,selectedItem: null});
  };
 
  handleCancel = () => {
    this.setState({ selectedItem: null,isDialogVisible: false });
  };

  handleAdd = (inputText) =>{
    if (inputText.trim() !== '') {
      const {todoItems} = this.props;
      if (this.state.selectedItem) {
        this.props.updateTodoItem({...this.state.selectedItem,todo:inputText});
      } else{
        this.props.addTodoItem({
          id: todoItems[todoItems.length-1] ? todoItems[todoItems.length-1].id + 1 : 1,
          todo: inputText,
        });
      }
    
    this.setState({isDialogVisible:false})
  }
}
deleteTodo = (item) => {
  this.props.deleteTodoItem(item);
}

editTodo = (item) => {
  console.log("editTodo",item)
  this.setState({selectedItem:item,isDialogVisible:true})
}
  todoListItem = ({item}) => {
    console.log("item",item)
    return (
      <ListItem>
        <Body>
          <Text>{item.todo}</Text>
        </Body>
        <Right>
          <View>
              <Text 
              onPress={() => {
                this.deleteTodo(item)
              }}>Delete</Text>
              <Text onPress={() => {this.editTodo(item)}}>Edit</Text>
          </View>
        </Right>
      </ListItem>
    );
  };
  
renderTodoList() {
    const {todoItems} = this.props;
    console.log('todoItems: ', todoItems)
    return (
      <FlatList
        data={todoItems}
        keyExtractor={item => item.id}
        renderItem={this.todoListItem}
      />
    );
  }

  

render() {
  return (
    <View style={{ flex: 1 }}>
      <View style={{alignItems:"center",fontWeight: 'bold'}}>
        <Text onPress = {() => {this.showDialog()}}> Add Todo </Text>
      </View>
      <DialogInput isDialogVisible={this.state.isDialogVisible}
          title={"Add to do"}
          hintInput ={"To do"}
          initValueTextInput={this.state.selectedItem ? this.state.selectedItem.todo : null}
          submitInput={ (inputText) => {this.handleAdd(inputText)} }
          closeDialog={this.handleCancel}>
      </DialogInput>
      {this.renderTodoList()}
    </View>
  );
}
}
const mapStateToProps = ( state , ownProps) => {

  const basicTodos = state.BasicReducer.todoItems
  const saurceTodos = state.ReduxSauceReducer.todoItems
  const method = ownProps.navigation.getParam('method')
  const todos =  method ? basicTodos : saurceTodos
  console.log('mapStateToProps: ', {method, basicTodos, saurceTodos, todos})
  return ({
   todoItems: todos,
})}

const mapDispatchToProps = ( dispatch,ownProps ) =>{
  const reduxMethod = ownProps.navigation.getParam('method');
  return {
    addTodoItem: todoItem => reduxMethod ? dispatch(addTodo(todoItem)) : dispatch(Creators.addTodo(todoItem)),
    updateTodoItem: todoItem => reduxMethod ? dispatch(updateTodo(todoItem)) : dispatch(Creators.updateTodo(todoItem)),
    deleteTodoItem: todoItem => reduxMethod ? dispatch(deleteTodo(todoItem)) : dispatch(Creators.deleteTodo(todoItem))
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
 )(TodoList);

