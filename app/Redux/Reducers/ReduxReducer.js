
import {ADD_TODO,UPDATE_TODO,DELETE_TODO} from "../Actions/Type";

const INITIAL_STATE = {
    todoItems: [],
}

export default (state=INITIAL_STATE,action) => {
    console.log("action",action);
    switch(action.type){
        
        case ADD_TODO:
        return {todoItems : [...state.todoItems, action.payload]};

        case UPDATE_TODO:
        const update_todoItem = [...state.todoItems];
        let id = update_todoItem.findIndex(data => data.id === action.payload.id);
        if( id > -1){
            console.log("state updated",id)
            update_todoItem[id] = {...action.payload}
            console.log("updatetodo",update_todoItem)
        }
        return {...state,todoItems:update_todoItem};

        case DELETE_TODO:
        const item = state.todoItems;
        console.log('item: ', item, 'action id: ', action.payload.id)
        const updatedItems = item.filter(data => data.id !== action.payload.id)
        console.log('updatedItems: ', updatedItems)
        const newState =  {todoItems: updatedItems}        
        console.log('newState: ', newState)

        return newState;

        default:
        return state;
    }
}
